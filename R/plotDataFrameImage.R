#' Plot an image converted from a data.frame
#'
#' This function converts a data.frame into a graphics::image by mapping its
#' values into different colours and then plots it.
#' A colour mappings can be defined for each column in the data.frame.
#' If not provided, a default grey-scale mapping is used.
#'
#' @param data The data.frame to be converted.
#' @param mapping A named list of colours (character vectors).
#'                The names should correspond to the column names of the data.frame.
#'                The colours are character vectors such as the output of `hcl.colors`.
#' @param xlab,ylab The x-axis,y-axis titles.
#' @param na.colour,na.color The colour (character) used for NA's. (na.colour takes precedence.)
#' @param show.rownames Whether or not to show the row names as x-axis labels.
#' @param show.ticks Whether or not to show ticks in the x-axis.
#' @param main.mar Margins (the `mar` parameter) for the main panel.
#' @param leg.mar Margins (the `mar` parameter) for the legends panels.
#' @param vline Add vertical line(s) after the specified columns.
#' @param vline.lty Line style for `vline`.
#' @param vline.lty Line width for `vline`.
#' @param vline.lty Line colour for `vline`.
#' @param ... Additional graphics parameters for the main plot (passed to `graphics::image`).
#' @return inv#' @param leg.mar Margins (the `mar` parameter) for the legends panels.

plotDataFrameImage <- function(data, mapping=list(),
                               xtitle="", ytitle="",
                               na.colour=NULL, na.color=NULL,
                               show.rownames=FALSE,
                               show.ticks=FALSE,
                               main.mar=NULL, leg.mar=NULL,
                               vline=NULL,
                               vline.lty=1, vline.lwd=1, vline.col="red",
                               ...) {

  colour_grid <- matrix(integer(0), nrow=nrow(data), ncol=ncol(data))
  colour_maps <- list()

  if(is.null(na.colour)) {
    if(is.null(na.color))
      na.colour <- "white"
    else
      na.colour <- na.color
  }
  na.colour <- na.colour[1]

  for(j in 1:ncol(data)) {
    colname <- colnames(data)[j]
    values <- data[,j]
    if(is.character(values)) values <- factor(values)
    current_map_length <- sum(vapply(colour_maps, length, FUN.VALUE=integer(1)))

    if(colname %in% names(mapping)) {
      colour_map <- mapping[[colname]]
    } else if(is.logical(values)) {
      colour_map <- c("lightgrey", "black")
    } else if(is.numeric(values)) {
      colour_map <- grey.colors(100, start=0.83, end=0)
    } else if(is.factor(values)) {
      if(is.character(values)) values <- factor(values)
      colour_map <- rainbow(nlevels(values))
    }

    if(is.logical(values)) {
      mapped_values <- values + current_map_length + 1
    } else if(is.numeric(values)) {
      min <- min(values, na.rm=TRUE)
      max <- max(values, na.rm=TRUE)
      normalised_values <- (values - min) / (max - min) * (length(colour_map)-1)
      mapped_values <- normalised_values + current_map_length + 1
    } else if(is.factor(values)) {
      mapped_values <- as.integer(values) + current_map_length
    } else {
      stop(sprintf("plotDataFrameImage: Cannot handle '%s' type data (%s)",
                   class(values), colname))
    }

    if(any(is.na(mapped_values))) {
      mapped_values[is.na(mapped_values)] = current_map_length + length(colour_map) + 1
      colour_map <- c(colour_map, na.colour)
    }

    colour_grid[,j] <- mapped_values
    colour_maps[[colname]] <- colour_map
  }

  combined_colour_map <- unlist(colour_maps, use.names=FALSE)

  layout(matrix(c(rep(1,ncol(data)+2), 2, 4:(ncol(data)+3), 3), ncol=2), widths=c(8,2))
  original_mar <- par("mar")

  # Main panel (layout #1)
  if(!is.null(main.mar)) par(mar=main.mar)
  else {
    warning("plotDataFrameImage: You did not specify main.mar! The main plot will be plotted without margins!")
    par(mar=c(0, 0, 0, 0))
  }
  image(z=colour_grid, x=1:nrow(data), y=1:ncol(data),
        col=combined_colour_map,
        zlim=c(0.5, length(combined_colour_map) + 0.5),
        xlab=xtitle, ylab=ytitle,
        axes=FALSE, ...)
  if(!is.null(vline)) {
    abline(v=vline+0.5, lty=vline.lty, lwd=vline.lwd, col=vline.col)
  }

  box()
  if(show.rownames) axis(1, at=1:nrow(data), labels=rownames(data), tick=show.ticks, las=2)
  else axis(1, at=1:nrow(data), labels=FALSE, tick=show.ticks)
  axis(2, at=1:ncol(data), labels=colnames(data), las=2)
  
  # Empty panels for better/easier alignment (layout #2,3)
  if(!is.null(leg.mar)) par(mar=leg.mar)
  else {
    warning("plotDataFrameImage: You did not specify leg.mar! The legends will be plotted without margins!")
    par(mar=c(0, 0, 0, 0))
  }
  plot.new()
  plot.new()

  # Legend panels (layout #4-(ncol+3))
  for(j in ncol(data):1) {
    values <- data[,j]
    colour_map <- colour_maps[[j]]
    if(any(is.na(values))) colour_map <- colour_map[-length(colour_map)]

    image(z=as.matrix(1:length(colour_map), ncol=1), x=1:length(colour_map), y=1,
          col=colour_map,
          zlim=c(0.5, length(colour_map) + 0.5),
          xlab="", ylab="", axes=FALSE)
    title(xlab=names(data)[j], line=1)
    box()

    if(is.logical(values)) {
      axis(1, at=c(1,2), labels=c("False", "True"), line=-1, tick=FALSE)
    }
    else if(is.numeric(values)) {
      axis(1, at=c(1,length(colour_map)),
           labels=c(min(values, na.rm=TRUE), max(values, na.rm=TRUE)),
           line=-1, tick=FALSE)
    }
    else if(is.factor(values) || is.character(values)) {
      if(is.character(values)) values <- factor(values)
      axis(1, at=1:length(colour_map), labels=levels(values), line=-1, tick=FALSE)
    }
  }

  par(mar=original_mar)

  return(invisible(NULL))
}
